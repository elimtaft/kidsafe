# KIDSAFE REPOSITORY

## Prerequisites (Debian)

You need to install Firefox and privoxy

## Instructions (Debian)

After installing Firefox and Privoxy, make sure privoxy is running:

    systemctl status privoxy

Now three files need to be copied.  For simplicity, just run this script:

    cd Debian
    ./apply.sh

## Prerequisites (Windows)

You need to install Firefox and CCProxy

## Instructions (Windows)

The files inside ProgrmFiles_MozilleFirefox are for Firefox.
You can probably just copy the mozilla.cfg file and the defaults folder right into the root directory of the Firefox program files directory.  Both of those files probably won't even exist yet for a fresh install of Firefox.  Otherwise, edit the existing manually.

The file inside the CCProxy folder is for importing the settings into CCProxy.
Just open CCProxy, go into Account, and "import"
