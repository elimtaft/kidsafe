#!/bin/sh

set -ex

cp etc/privoxy/user.action /etc/privoxy/
cp usr/lib/firefox-esr/defaults/pref/local-settings.js /usr/lib/firefox-esr/defaults/pref/
cp usr/lib/firefox-esr/mozilla.cfg /usr/lib/firefox-esr/
systemctl restart privoxy
